using System.Collections.Generic;
using System;

namespace Tennis
{
    public class TennisGame2 : ITennisGame
    {
        private string p1Name, p2Name;
        private int p1Points, p2Points;

        private Dictionary<int, string> pointName = new Dictionary<int, string>()
        {
            [0] = "Love",
            [1] = "Fifteen",
            [2] = "Thirty",
            [3] = "Forty"
        };

        public TennisGame2(string player1Name, string player2Name)
        {
            p1Name = player1Name;
            p2Name = player2Name;
            p1Points = p2Points = 0;
        }

        /// Returns winners' name if game is won or null if no winner yet.
        public string GetWinner()
        {
            Func<int, int, bool> winPoints = (p1, p2) => p1 >= 4 && p1 - p2 >= 2;
            return winPoints(p1Points, p2Points) ? p1Name :
                   winPoints(p2Points, p1Points) ? p2Name :
                   null;
        }

        public string GetScore()
        {
            var winner = GetWinner();
            if(winner != null)
                return $"Win for {winner}";
            var isDeuceGame = p1Points >= 3 && p2Points >= 3;
            if(p1Points == p2Points)
                return isDeuceGame ? "Deuce" : $"{pointName[p1Points]}-All";
            if(isDeuceGame)
            {
                var playerAhead = p1Points > p2Points ? p1Name : p2Name;
                return $"Advantage {playerAhead}";
            }
            return $"{pointName[p1Points]}-{pointName[p2Points]}";
        }

        public void WonPoint(string player)
        {
            if(p1Name == player)
                p1Points += 1;
            else if(p2Name == player)
                p2Points += 1;
            else 
                throw new ArgumentException("No such player");
        }
    }
}
